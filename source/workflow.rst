Workflow
========

The workflow of the pipeline is shown below:

.. figure:: _static/metafunc_workflow.png

      MetaFunc workflow


The pipeline is executed in order below starting with sequencing reads in fasta- or fastq-format stemming from a transcriptomic or genomic experiment.
It is also indicated if a step is optional - there is an option in the config file not to go through with the step.

.. _pre-processing:

**Initial processing of inputs** (*OPTIONAL*)

.. note::  
  The entire pre-processing of inputs is optional. A user can start at any point in the 3 steps below. 
  For **step 3**, one can either use unmapped reads from **step 2** or directly input reads to
  the :ref:`microbiome analysis <microbe-ana>` if the reads are already of satisfactory quality
  and do not require adapter and/or host read removal.

1.    Use `fastp <https://github.com/OpenGene/fastp>`__ for adapter trimming and quality control 
2.    Use trimmed reads in `STAR <https://github.com/alexdobin/STAR>`_ to map to a host genome
3.    Use (unmapped) reads as input to `Kaiju <http://kaiju.binf.ku.dk>`_

.. _host-ana:

**Host Analysis** (*OPTIONAL*)

.. note::  
  Reads are from **step 2** of :ref:`Initial processing of inputs <pre-processing>`. DGEA and GSEA are only performed if comparisons between groups are indicated.
  To perform a GSEA an additional gene set file in GMT-format is needed (a human Gene Ontology file is included as an example). 

1.    Use `featureCounts` of the `subread <http://subread.sourceforge.net>`_ package to quantify gene counts
2.    Use gene count information to perform DGEA using `edgeR <https://bioconductor.org/packages/release/bioc/html/edgeR.html>`__. (*OPTIONAL*)
3.    Use information obtained from DGEA to perform GSEA using `clusterProfiler <https://bioconductor.org/packages/release/bioc/html/clusterProfiler.html>`__. (*OPTIONAL*)

.. _microbe-ana:

**Microbiome Analysis**

.. note::  
  Reads are from **step 1 or 3** of :ref:`Initial processing of inputs <pre-processing>`. 
  This part of the workflow is mandatory but analyses based on group/condition information are optional.

1.    Taxonomy and protein matches per read are identified using `Kaiju <http://kaiju.binf.ku.dk>`_.
2.    Taxonomy ID counts per sample are summarized into a table per sample. 

      a.    Table with Taxonomy ID counts per group is generated (*OPTIONAL*).
      
      b.    Use TaxID and group information to get differentially abundant species using `edgeR <https://bioconductor.org/packages/release/bioc/html/edgeR.html>`__. (*OPTIONAL*)

3.    Use protein information to get gene ontology (GO) annotations for each sample. 

      a.    Tables with GOs per group/condition are generated (*OPTIONAL*).

**Host - Microbiome Relationship** (*OPTIONAL*)

.. note::  
  Input to this step are the top DGEs (specifiable in `(4h)` of the :ref:`label-config`) from **step 2** of :ref:`Host Analysis <host-ana>` and 
  top DA species (specifiable in `(4h)` of the :ref:`label-config`) from **step 2b** of :ref:`Microbiome Analysis <microbe-ana>`.

1.    Spearman correlation analysis between DEGs of the host gene analysis and DA species of the microbiome taxonomy analysis is performed 
2.    Results are summarized in a matrix and clustered using `clustergrammer <http://amp.pharm.mssm.edu/clustergrammer>`__.