
.. _label-logs:

=========
Log files
=========

This page contains information on some log files that may be important for the user to look at.

Taxonomy ID Tables
------------------

Log files for TaxID information may be found at the bottom of the file ``logs/microbiome/taxonomy/all.tax.out``. 
It has the following format:

+---------------------+--------------+--------------+---------+--------------+
| Basic_Information   | Sample 1     | Sample 2     | ...     | Sample N     |
+=====================+==============+==============+=========+==============+
| all_Species         | ###          | ###          | ...     | ###          |
+---------------------+--------------+--------------+---------+--------------+
| FilteredSpecies     | ###          | ###          | ...     | ###          |
+---------------------+--------------+--------------+---------+--------------+
| Higher_Taxon_Levels | ###          | ###          | ...     | ###          |
+---------------------+--------------+--------------+---------+--------------+
| Others              | ###          | ###          | ...     | ###          |
+---------------------+--------------+--------------+---------+--------------+
| Unclassified        | ###          | ###          | ...     | ###          |
+---------------------+--------------+--------------+---------+--------------+
| Total               | ###          | ###          | ...     | ###          |
+---------------------+--------------+--------------+---------+--------------+

**Description of above rows**

    - **all_Species:** Sum of all reads that have classifications at the species-level and belonging to |kaiju-taxonlistEuk.tsv|. These include only species that have passed cutoff.
    - **FilteredSpecies:** Sum of all reads that have been classified to species that did not pass cutoff.
    - **Higher_Taxon_Levels:** Sum of all reads classified within |kaiju-taxonlistEuk.tsv| but not at species-level.
    - **Others:** Sum of all reads with classification but not within |kaiju-taxonlistEuk.tsv| (e.g. 'cellular organisms').
    - **Unclassified:** Sum of all reads that |kaiju| cannot classify.
    - **Total:** Sum of all rows above. Should equate to number of input reads.

.. note::

    Above sums are based on raw read counts.

GO Tables
---------

Log files for GO information may be found at the bottom of files with path ``logs/microbiome/source/go/*/*.go.out``. This could be for information for individual samples or for each group (or for *ALL*, where all samples are gathered as one group).
This information is also given out per taxa of interest chosen in the configuration file, or for all taxa in |kaiju-taxonlistEuk.tsv|. It has the following format:

+-------------------------+----------------+----------------+---------+----------------+
| Basic_Information       | Sample/Group 1 | Sample/Group 2 | ...     | Sample/Group N |
+=========================+================+================+=========+================+
| Accessions_with_GO      | ###            | ###            | ...     | ###            |
+-------------------------+----------------+----------------+---------+----------------+
| Accessions_without_GO   | ###            | ###            | ...     | ###            |
+-------------------------+----------------+----------------+---------+----------------+
| Reads_with_GO           | ###            | ###            | ...     | ###            |
+-------------------------+----------------+----------------+---------+----------------+
| Reads_without_GO        | ###            | ###            | ...     | ###            |
+-------------------------+----------------+----------------+---------+----------------+
| Reads_below_cutoff_file | ###            | ###            | ...     | ###            |
+-------------------------+----------------+----------------+---------+----------------+

**Description of above rows**

    - **Accessions_with_GO:** Number of unique accession numbers with GO annotations.
    - **Accessions_without_GO:** Number of unique accession numbers with no available GO annotations.
    - **Reads_with_GO:** Number of reads that match to accession numbers with GO annotations.
    - **Reads_without_GO:** Number of reads that match to accession numbers with no available GO annotations.
    - **Reads_below_cutoff_file:** Number of reads that match to accession numbers belonging to taxonomies that did not pass cutoff. Reads and accession numbers of the rows above belong to proteins from taxonomies that have passed cutoff.
