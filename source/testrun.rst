
Test Run
========

To test the the workflow, we recommend the following steps:

Install Dependencies
--------------------

|miniconda| is used to create software environments necessary to run the workflow. The workflow is then run using the workflow engine |snakemake|.
In order to run the workflow with |singularity|, a working |singularity| 3.5 installation is needed. For details installing these dependencies, see 
:ref:`label-dep`.


Download the workflow
---------------------

.. code:: bash

      $ git clone https://gitlab.com/schmeierlab/workflows/metafunc.git && cd metafunc

Create a mapping reference index of the host (here human)
---------------------------------------------------------

.. code:: bash

      # download host genome
      wget ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_33/GRCh38.primary_assembly.genome.fa.gz
      gzip -d GRCh38.primary_assembly.genome.fa.gz

      # download corresponding gene annotation
      wget ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_33/gencode.v33.primary_assembly.annotation.gtf.gz
      gzip -d gencode.v33.primary_assembly.annotation.gtf.gz

      # build STAR index
      conda create --yes -n star star=2.7.3a
      conda activate star
      mkdir genome
      STAR --runMode genomeGenerate --runThreadN 12 --genomeDir genome --genomeFastaFiles GRCh38.primary_assembly.genome.fa

Download provided Kaiju Database
--------------------------------

.. code:: bash

       wget https://zenodo.org/record/5602178/files/kaijudb_nreuk_202001.tar.bz2
       tar xvjf kaijudb_nreuk_202001.tar.bz2 && rm kaijudb_nreuk_202001.tar.bz2

Download provided nr-go database for ``nr_euk``:
------------------------------------------------

.. code:: bash

       wget https://zenodo.org/record/5602157/files/nrgo_202001_updated.tar.bz2 
       tar xvjf nrgo_202001_updated.tar.bz2 && rm nrgo_202001_updated.tar.bz2

Run the workflow
----------------

The config-file is setup to work with the test-data in a ``.test`` directory within the workflow, using only *Viruses* for microbiome functional output.
Run the workflow either in conda-only or singularity-only mode:

.. code:: bash

        # conda-mode
        snakemake -p --use-conda --cores 12 --resources const=1 --configfile config.yaml > run.log 2>&1

        # singularity-mode
        snakemake -p --use-singularity --cores 12 --resources const=1 --configfile config.yaml > run.log 2>&1

       













