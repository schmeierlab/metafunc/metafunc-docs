# MetaFunc Docs / Sphinx

[![Documentation Status](https://readthedocs.org/projects/metafunc/badge/?version=latest)](https://metafunc.readthedocs.io/en/latest/?badge=latest) [![pipeline status](https://gitlab.com/schmeierlab/metafunc/metafunc-docs/badges/master/pipeline.svg)](https://gitlab.com/schmeierlab/metafunc/metafunc-docs/-/commits/master)

Repo for the documentation of the MetaFunc workflow/pipeline.
The documentation is available at: [https://metafunc.readthedocs.io](https://metafunc.readthedocs.io/en/latest/?badge=latest).
  
## Installing Locally for developement

1. Set up a [python virtual environment](https://packaging.python.org/guides/installing-using-pip-and-virtualenv/)
   named `venv` => `pip install virtualenv; virtualenv --python=python3.5 venv`.
2. Activate the `venv` environment => `source venv/bin/activate`
3. Install the dependencies inside of it by running  `pip install -r requirements_dev.txt`.
4. Run `make htmlwatch`.
5. Edit your rst-files.
6. Commit changes
7. Bump version using, e.g. `bump2version patch`
8. Push changes to remote
